import React from "react";
import styled from "styled-components";

export const Text = styled.span`
  margin-left: 1rem;
  color: #fff;
`;

export const Wrap = styled.div`
  ${p => (p.marginLess ? `margin: 0;` : `margin-bottom: 2rem;`)};
`;

export const Checkbox = props => {
  return (
    <Wrap marginLess={props.marginLess}>
      <input type="checkbox" />
      <Text>{props.text}</Text>
    </Wrap>
  );
};
