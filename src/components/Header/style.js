import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledHeader = styled.header`
  display: flex;
  justify-content: space-between;
  box-shadow: 0px 12px 22px rgba(0, 0, 0, 0.24);
  background: #2b262d;
`;

export const Navigation = styled.nav`
  padding: 1rem 2rem;
`;

export const NavList = styled.ul`
  margin: 0;
  padding-left: 0;
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
`;

export const NavListItem = styled.li`
  padding-left: 30px;
`;

export const Logo = styled(Link)`
  color: #302b32;
  font-size: 2rem;
  text-decoration: none;
  padding: 0.5rem 2rem;
  background: #e06764;

  display: flex;
  align-items: center;
`;

export const StyledLink = styled(Link)`
  color: #fff;
  text-decoration: none;
  transition: 0.2s all ease-in-out;
  border-bottom: 2px solid transparent;
  :hover {
    border-bottom: 2px solid #fff;
  }
`;
