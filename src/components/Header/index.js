import React from "react";
import {
  StyledHeader,
  Logo,
  Navigation,
  NavList,
  NavListItem,
  StyledLink
} from "./style";

class Header extends React.Component {
  static defaultProps = {};

  state = {};

  render() {
    return (
      <StyledHeader>
        <Logo to="/">ShopLogo</Logo>
        <Navigation>
          <NavList>
            <NavListItem>
              <StyledLink to="/categoryList">Category List</StyledLink>
            </NavListItem>
            <NavListItem>
              <StyledLink to="/login">Login</StyledLink>
            </NavListItem>
            <NavListItem>
              <StyledLink to="/registration">Registration</StyledLink>
            </NavListItem>
          </NavList>
        </Navigation>
      </StyledHeader>
    );
  }
}

export default Header;

Header.propTypes = {};
