import styled from 'styled-components';   

export const StyledForm = styled.form`
    border-radius: 3px;
    padding: 60px;
    width: ${p => p.width}px;
    background: #363138;
    box-shadow: 0px 0px 20px #2A262B;
`;

export const FormTitle = styled.h2`
  color: #E06764;
  margin-bottom: 3rem;
  margin-top: 0;
`;

export const FormGroup = styled.div`
  margin-bottom: 2rem;
  display: flex;
  flex-flow: column nowrap;
`;

export const FormLabel = styled.label`
  margin-bottom: .5rem;
  color: #fff;
`;