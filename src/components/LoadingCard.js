import React from "react";
import { Text, Box } from "rebass";

export const LoadingCard = props => {
  return (
    <Box p={4}>
      <Text color="#fff">Loading...</Text>
    </Box>
  );
};
