import React from 'react';
import styled from 'styled-components';
import {Button} from 'rebass'; 

const StyledButton = styled(Button)`
  && {
    border-radius: 30px;
    background-color: #E06764;
    color: #fff;
    padding: 10px 20px;
    font-size: 1rem;
    margin: 0 auto;
    outline: none;
    border: none;
    cursor: pointer;
    :focus {
        box-shadow: none;
    }
  }
`;

export const CustomButton = (props) => {
    return (
        <StyledButton>{props.text}</StyledButton>
    )
}