import React from "react";
import styled from "styled-components";

export const StyledInput = styled.input`
  padding: 15px;
  border-radius: 3px;
  outline: none;
  border: 1px solid #cccccc;
`;

class CustomInput extends React.Component {
  state = {};
  render() {
    return (
      <StyledInput
        placeholder={this.props.placeholder}
        name={this.props.name}
        type={this.props.type}
      />
    );
  }
}

export default CustomInput;
