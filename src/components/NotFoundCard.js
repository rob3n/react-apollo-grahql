import React from "react";
import { Text, Box } from "rebass";

export const NotFoundCard = props => {
  return (
    <Box p={4}>
      <Text color="#fff">Categories not found...</Text>
    </Box>
  );
};
