import React from "react";
import { Provider } from "react-redux";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import "./App.css";
import Routes from "./routes";
import store from "./store";

// setup
const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <React.Fragment>
          <Routes />
        </React.Fragment>
      </Provider>
    </ApolloProvider>
  );
};

export default App;
