import React from 'react';
import {Switch, BrowserRouter, Route} from 'react-router-dom';
import Login from '../pages/Login';
import Main from '../pages/Main';
import Registration from '../pages/Registration';
import CategoryList from '../pages/CategoryList';
import Product from '../pages/Product';
import ProductList from '../pages/ProductList';
import Header from '../components/Header';


class Routes extends React.Component {
  state = {  }

  componentDidMount() {

  }

  render() {
    return (
      <BrowserRouter>
          <React.Fragment>
          <Header></Header>
          <Switch>
            <Route exact path="/" component={Main}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/registration" component={Registration}/>
            <Route exact path="/categoryList" component={CategoryList}/>
            <Route exact path="/productList/:categoryId" component={ProductList}/>
            <Route exact path="/product/:productId" component={Product}/>
          </Switch>
          </React.Fragment>
        </BrowserRouter>
    );
  }
}

export default Routes;