import React from "react";
import { Flex, Box } from "rebass";
import { Row, DescCard, CardTitle, CardKey, CardValue } from "./style";

class Product extends React.Component {
  static defaultProps = {};

  state = {};

  render() {
    return (
      <Flex mx={4} my={4}>
        <Box width={1 / 2} px={2}>
          <DescCard>
            <CardTitle>Kapusta</CardTitle>
            <Row>
              <CardKey>Price</CardKey>
              <CardValue>1000 $</CardValue>
            </Row>
          </DescCard>
        </Box>
        <Box width={1 / 2} px={2}>
          <DescCard>
            <CardTitle>Compnaiya Star</CardTitle>
            <Row>
              <CardKey>Years</CardKey>
              <CardValue>33</CardValue>
            </Row>
          </DescCard>
        </Box>
      </Flex>
    );
  }
}

export default Product;

Product.propTypes = {};
