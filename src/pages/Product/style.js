import styled from "styled-components";

export const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 0.5rem;
`;

export const DescCard = styled.div`
  padding: 20px;
  border-radius: 3px;
  flex: 0 0 400px;
  color: #fff;
  background: #363138;
`;

export const CardTitle = styled.h2`
  margin-bottom: 1rem;
`;

export const CardKey = styled.span``;

export const CardValue = styled.span``;
