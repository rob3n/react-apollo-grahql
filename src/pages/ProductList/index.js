import React from "react";
import { Box, Subhead } from "rebass";
import { List, ListItem, StyledCard } from "./style";

const productsData = [
  {
    id: 1,
    title: "one"
  },
  {
    id: 2,
    title: "two"
  },
  {
    id: 3,
    title: "three"
  },
  {
    id: 4,
    title: "four"
  }
];

class ProductList extends React.Component {
  static defaultProps = {};

  state = {};

  render() {
    const renderArrProducts = productsData.map(product => (
      <ListItem key={product.id} to={`/product/${product.id}`}>
        <Box>
          <StyledCard bg="#363138">
            <Box p={1} color="white">
              <Subhead>{product.title}</Subhead>
            </Box>
          </StyledCard>
        </Box>
      </ListItem>
    ));
    return <List>{renderArrProducts}</List>;
  }
}

export default ProductList;

ProductList.propTypes = {};
