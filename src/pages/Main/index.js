import React from "react";
import { Link, withRouter } from "react-router-dom";

class Main extends React.Component {
  state = {};

  componentDidMount() {
    const { path } = this.props.match;
    const { push } = this.props.history;

    if (path === "/") {
      push("/categoryList");
    }
  }

  render() {
    return (
      <div>
        <Link to="/login">Hello Main</Link>
      </div>
    );
  }
}

export default withRouter(Main);
