import React from "react";
import LoginForm from "./LoginForm";
import { PageWrap } from "../pagesStyles";

export const Login = () => {
  return (
    <PageWrap>
      <LoginForm />
    </PageWrap>
  );
};

export default Login;
