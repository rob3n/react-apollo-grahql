import React from "react";
import {
  StyledForm,
  FormTitle,
  FormGroup,
  FormLabel
} from "../../components/formStyles";
import Input from "../../components/Input";
import { Checkbox } from "../../components/Checkbox";
import { CustomButton } from "../../components/Button";

class RegisterForm extends React.Component {
  static defaultProps = {};

  state = {};

  render() {
    return (
      <StyledForm width={500}>
        <FormTitle>Log in</FormTitle>
        <FormGroup>
          <FormLabel htmlFor="user">Username</FormLabel>
          <Input name="user" />
        </FormGroup>
        <FormGroup>
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input type="password" name="password" />
        </FormGroup>
        <div
          style={{
            display: "flex",
            flexFlow: "row nowrap",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Checkbox marginLess text="I have read the policy" />
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <CustomButton text="Submit" />
          </div>
        </div>
      </StyledForm>
    );
  }
}

export default RegisterForm;

RegisterForm.propTypes = {};
