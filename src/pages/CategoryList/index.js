import React from "react";
import { Box, Subhead, Small } from "rebass";
import { gql } from "apollo-boost";
import { graphql } from "react-apollo";

import { List, ListItem, StyledCard } from "./style";
import { LoadingCard } from "../../components/LoadingCard";
import { NotFoundCard } from "../../components/NotFoundCard";

const getArticles = gql`
  {
    articles {
      id
      description
      title
    }
  }
`;

class CategoryList extends React.Component {
  static defaultProps = {};

  state = {};

  render() {
    const { data } = this.props;

    if (data.loading) {
      return <LoadingCard />;
    } else if (data.error) {
      return <NotFoundCard />;
    }

    const renderCategories = data.articles.map(article => (
      <ListItem key={article.id} to={`/productList/${article.id}`}>
        <Box>
          <StyledCard p={0} bg="#363138">
            <Box pt={3} pl={3} pb={2} color="white">
              <Subhead mb={2}>{article.title}</Subhead>
              <Small>{article.description}</Small>
            </Box>
          </StyledCard>
        </Box>
      </ListItem>
    ));

    return <List>{renderCategories}</List>;
  }
}

export default graphql(getArticles)(CategoryList);

CategoryList.propTypes = {};
