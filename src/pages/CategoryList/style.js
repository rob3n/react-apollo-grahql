import styled from "styled-components";
import { Card } from "rebass";
import { Link } from "react-router-dom";

export const List = styled.ul`
  padding: 2rem;
  margin: 0;
  list-style: none;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
`;

export const ListItem = styled(Link)`
  flex: 0 0 33%;
  padding: 1rem;
  border-radius: 3px;
  box-sizing: border-box;
  margin-bottom: 1rem;
  text-decoration: none;
`;

export const CardTitle = styled.h2`
  margin: 0 0 1rem;
`;

export const CardDesc = styled.p`
  margin: 0;
`;

export const StyledCard = Card.extend`
  transition: 0.2s all ease-in-out;
  :hover {
    box-shadow: 0 12px 24px rgba(0, 0, 0, 0.25);
  }
`;
