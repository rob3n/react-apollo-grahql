import styled from 'styled-components';

export const PageWrap = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 70px 0;
`;