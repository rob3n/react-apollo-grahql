import React from "react";
import {
  StyledForm,
  FormTitle,
  FormGroup,
  FormLabel
} from "../../components/formStyles";
import Input from "../../components/Input";
import { Checkbox } from "../../components/Checkbox";
import { CustomButton } from "../../components/Button";

class RegisterForm extends React.Component {
  static defaultProps = {};

  state = {};

  render() {
    return (
      <StyledForm width={500}>
        <FormTitle>Registration</FormTitle>

        <FormGroup>
          <FormLabel htmlFor="username">Username</FormLabel>
          <Input name="username" />
        </FormGroup>
        <FormGroup>
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input type="email" name="email" />
        </FormGroup>
        <FormGroup>
          <FormLabel htmlFor="phone">Phone</FormLabel>
          <Input type="number" name="phone" />
        </FormGroup>
        <FormGroup>
          <FormLabel htmlFor="password">Password</FormLabel>
          <Input type="password" name="password" />
        </FormGroup>
        <Checkbox text="i have read the policy" />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <CustomButton text="Submit" />
        </div>
      </StyledForm>
    );
  }
}

export default RegisterForm;

RegisterForm.propTypes = {};
