import React from "react";
import { PageWrap } from "./../pagesStyles";
import RegisterForm from "./RegisterForm";

class Registration extends React.Component {
  state = {};

  render() {
    return (
      <PageWrap>
        <RegisterForm />
      </PageWrap>
    );
  }
}

export default Registration;
