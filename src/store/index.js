import {createStore} from 'redux';
import reducer from 'redux-form/lib/reducer';

const store = createStore(reducer);

export default store;