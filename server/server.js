const express = require("express");
const express_graphql = require("express-graphql");
const { buildSchema } = require("graphql");
const cors = require("cors");

const articlesData = [
  {
    id: 1,
    title: "The Complete Node.js Developer Course",
    description:
      "Learn Node.js by building real-world applications with Node, Express, MongoDB, Mocha, and more!"
  },
  {
    id: 2,
    title: "Node.js, Express & MongoDB Dev to Deployment",
    description:
      "Learn by example building & deploying real-world Node.js applications from absolute scratch"
  },
  {
    id: 3,
    title: "JavaScript: Understanding The Weird Parts",
    description:
      "An advanced JavaScript course for everyone! Scope, closures, prototypes, this, build your own framework, and more."
  }
];

const schema = buildSchema(`
    type Query {
        article(id: Int!): Article
        articles(description: String): [Article]
    }
    type Article {
        id: Int
        title: String
        description: String
    }
`);

const getArticle = function(args) {
  const id = args.id;
  return articlesData.filter(course => {
    return course.id === id;
  })[0];
};

const getArticles = function(args) {
  if (args.topic) {
    const description = args.description;
    return articlesData.filter(course => course.description === description);
  }
  return articlesData;
};

const root = {
  article: getArticle,
  articles: getArticles
};

// create server
const app = express();

app.use(cors());

app.use(
  "/graphql",
  express_graphql({
    schema,
    rootValue: root,
    graphiql: true
  })
);

app.listen(4000, () =>
  console.log("Express server with graphql running on 4000")
);
